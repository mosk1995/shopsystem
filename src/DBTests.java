import dbinteraction.ConnectDB;
import entity.Product;
import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by Xenomorf on 06.05.2015.
 */
public class DBTests {


    @Test
    public void test1() {
        ConnectDB test = new ConnectDB();
        Random random = new Random();
        Product h = new Product("12a4567b91c45", "SomeProduct", 10, 10);
        test.insertProduct(h);
        Product p = test.findProductBarcode(h.getBarcode());
        assertNotNull(p);
    }

    @Test
    public void test2() {
        ConnectDB test = new ConnectDB();
        Random random = new Random();
        Product h = new Product("12345678913452hgjergrehntgujerhtuiwerheurhtiurth", "SomeProduct", 10, 10);
        test.insertProduct(h);
        Product p = test.findProductBarcode(h.getBarcode());
        assertNull(p);
    }
    @Test
    public void test3() {
        ConnectDB test = new ConnectDB();
        Random random = new Random();
        Product h = new Product("123456789134a", "**!!?", 10, 10);
        test.insertProduct(h);
        Product p = test.findProductBarcode(h.getBarcode());
        assertNull(p);
    }



    @Test
    public void test4() {
        ConnectDB test = new ConnectDB();
        Random random = new Random();
        Product h = new Product("123456789134b", "", 10, 10);
        test.insertProduct(h);
        Product p = test.findProductBarcode(h.getBarcode());
        assertNull(p);
    }
    @Test
    public void test5(){
        ConnectDB test = new ConnectDB();
        Random random = new Random();
        Product h = new Product("123456789134cgvf", "", 10, 10);
        test.insertProduct(h);
        Product p = test.findProductBarcode(h.getBarcode());
        assertNull(p);
    }

    @Test
    public void test6(){
        ConnectDB test = new ConnectDB();
        Random random = new Random();
        Product h = new Product("1345at85eq23", "SomeProduct", 10, 10);
        test.insertProduct(h);
        Product p = test.findProductBarcode(h.getBarcode());
        assertNull(p);
    }

    @Test
    public  void test7(){
        ConnectDB test = new ConnectDB();
        Random random = new Random();
        Product h = new Product(" *!*..:%;№»..?", "SomeProduct", 10, 10);
        test.insertProduct(h);
        Product p = test.findProductBarcode(h.getBarcode());
        assertNull(p);
    }

    @Test
    public  void test8(){
        ConnectDB test = new ConnectDB();
        Random random = new Random();
        Product h = new Product("", "SomeProduct", 10, 10);
        test.insertProduct(h);
        Product p = test.findProductBarcode(h.getBarcode());
        assertNull(p);
    }

    @Test
     public  void test9(){
        ConnectDB test = new ConnectDB();
        Random random = new Random();
        Product h = new Product("987654321123h", "SomeProduct", -10, 10);
        test.insertProduct(h);
        Product p = test.findProductBarcode(h.getBarcode());
        assertNull(p);
    }


    @Test
    public void test10() {
        ConnectDB test = new ConnectDB();
        Random random = new Random();
        Product h = new Product("asdfghjklzxcv", "SomeProduct", 10, -10);
        test.insertProduct(h);
        Product p = test.findProductBarcode(h.getBarcode());
        assertNull(p);
    }

    @Test
    public void test11() {
        ConnectDB test = new ConnectDB();
        test.deleteProduct("12a4567b91c45");
        Product p = test.findProductBarcode("12a4567b91c45");
        assertNull(p);
    }




}
