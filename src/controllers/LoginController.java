package controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;

import dbinteraction.*;

public class LoginController extends GridPane {
    public static String USER_NAME="ANANAS";
    @FXML
    Button authButton;
    @FXML
    Label errorLogin;
    @FXML
    Label errorPass;
    @FXML
    PasswordField textPassword;
    @FXML
    TextField textLogin;
    @FXML
    ComboBox comboRole;
    @FXML
    Button helpButton;

    public LoginController() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/views/LoginGUI.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
            comboRole.getItems().addAll("Кассир", "Мерчендайзер", "Кладовщик");
            comboRole.getSelectionModel().select(0);
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    @FXML
    public void handleLogin(ActionEvent event) {
        errorLogin.setText("");
        errorPass.setText("");
        ConnectDB connector = new ConnectDB();
        //textLogin.setText("main_worker");
        //textPassword.setText("1234");
        String login = textLogin.getText();
        String password = textPassword.getText();
        int selected = comboRole.getSelectionModel().getSelectedIndex();

        if (connector.checkName(login)) {
            if (connector.checkPass(login, password)) {
                String access = connector.getAccess(login, password);
                USER_NAME=login;
                switch (selected) {
                    case 0:
                        if (access.contains("c")) {
                            CashierController cashierController=new CashierController();
                            Parent root;
                            Stage stage = new Stage();
                            stage.setTitle("Кассовый аппарат");
                            stage.setScene(new Scene(cashierController, 700, 500));
                            stage.setResizable(false);
                            stage.show();
                            ((Node)(event.getSource())).getScene().getWindow().hide();

                            break;
                        } else {
                            errorLogin.setText("У пользователя нет прав");
                            break;
                        }

                    case 1:
                        if (access.contains("m")) {
                            MerchandiserController merchandiserController=new MerchandiserController();

                            Stage stage = new Stage();
                            stage.setTitle("Торговый зал");
                            stage.setScene(new Scene(merchandiserController, 300, 250));
                            stage.setResizable(false);
                            stage.show();
                            ((Node)(event.getSource())).getScene().getWindow().hide();
                            break;
                        } else {
                            errorLogin.setText("У пользователя нет прав");
                            break;
                        }

                    case 2:
                        if (access.contains("w")) {
                            WarehouseController warehouseController=new WarehouseController();

                            Stage stage = new Stage();
                            stage.setTitle("Склад");
                            stage.setScene(new Scene(warehouseController, 700, 500));
                            stage.setResizable(false);
                            stage.show();
                            ((Node)(event.getSource())).getScene().getWindow().hide();
                            break;
                        } else {
                            errorLogin.setText("У пользователя нет прав");
                            break;
                        }
                    default:
                        break;
                }
                ;
            } else {
                errorPass.setText("Неверный пароль");
            }

        } else {
            errorLogin.setText("Отстутствует пользователь с таким именем");
        }


    }

    @FXML
    public  void handleHelp(ActionEvent event){
        HelpController helpController=new HelpController();
        Stage stage = new Stage();
        stage.setTitle("Справка");
        stage.setScene(new Scene(helpController, 1000, 700));
        stage.setResizable(true);
        stage.show();
    }



}


