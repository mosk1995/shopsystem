package controllers;

import dbinteraction.ConnectDB;
import entity.Product;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.layout.VBox;

import java.io.IOException;

/**
 * Created by KishMishNev on 23.04.2015
 */
public class MerchandiserController extends VBox{
    @FXML
    Button buttonDelete;
    @FXML
    Label labelNameMerchandiser;
    @FXML
    TextArea textBarcode;
    @FXML
    TextArea textCount;
    @FXML
    Label labelMerchError;

    @FXML
    public void handlerDelete(ActionEvent actionEvent){
        ConnectDB connectDB=new ConnectDB();
        String barcode=textBarcode.getText();
        labelMerchError.setText("");
        String countString=textCount.getText();
        int count=-1;
        try{
            Product product=null;
            count=Integer.parseUnsignedInt(countString);
            product=connectDB.findProductBarcode(barcode);
            if (product!=null){
                if (count>=product.getCount()){
                    connectDB.setCountProduct(barcode, 0);
                } else {
                    connectDB.setCountProduct(barcode, product.getCount() - count);
                }

            } else {
            }


        } catch (NumberFormatException e){
            labelMerchError.setText("Ошибка в количестве списываемых товаров");
            System.out.println("Новый изменения");
        }



    }

    MerchandiserController(){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/views/MerchandiserGUI.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
            labelNameMerchandiser.setText(LoginController.USER_NAME);
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }
}
