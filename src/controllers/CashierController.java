package controllers;

import dbinteraction.ConnectDB;
import entity.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;

import java.io.IOException;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;

/**
 * Created by Xenomorf on 08.04.2015.
 */
public class CashierController extends VBox {
    @FXML
    TableView<Product> tableCheck;
    @FXML
    Label labelCashierName;
    @FXML
    Button buttonAddRandProduct;
    @FXML
    Button buttonEnterCount;
    @FXML
    Button buttonEnterBarcode;
    @FXML
    Button buttonDiscountCard;
    @FXML
    Button buttonCloseCheck;
    @FXML
    Button buttonOpenCheck;
    @FXML
    Label labelCassEndMessage;
    @FXML
    Label labelCassEndValue;
    @FXML
    Button buttonDeleteProduct;
    @FXML
    Parent root;


    private ObservableList<Product> check = FXCollections.observableArrayList();
    private double result = 0;
    private double maxDiscount=0;
    boolean isOpened = false;


    public CashierController() {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/views/CashierGUI.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
            labelCashierName.setText(LoginController.USER_NAME);
            tableCheck.setEditable(false);
            tableCheck.getColumns().remove(0, 2);
            TableColumn barcodeColumn = new TableColumn("Штрих-код");
            barcodeColumn.setPrefWidth(100);
            barcodeColumn.setResizable(false);
            barcodeColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("barcode"));
            TableColumn nameColumn = new TableColumn("Название");
            nameColumn.setPrefWidth(150);
            nameColumn.setResizable(false);
            nameColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("name"));
            TableColumn priceColumn = new TableColumn("Цена");
            priceColumn.setPrefWidth(60);
            priceColumn.setResizable(false);
            priceColumn.setCellValueFactory(new PropertyValueFactory<Product, Double>("cost"));
            TableColumn countColumn = new TableColumn("Количество");
            countColumn.setPrefWidth(100);
            countColumn.setResizable(false);
            countColumn.setCellValueFactory(new PropertyValueFactory<Product, Integer>("count"));
            TableColumn endSumColumn = new TableColumn("Итого");
            endSumColumn.setPrefWidth(93);
            endSumColumn.setResizable(false);
            endSumColumn.setCellValueFactory(new PropertyValueFactory<Product, Double>("endSum"));

            tableCheck.getColumns().add(barcodeColumn);
            tableCheck.getColumns().add(nameColumn);
            tableCheck.getColumns().add(priceColumn);
            tableCheck.getColumns().add(countColumn);
            tableCheck.getColumns().add(endSumColumn);


        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    @FXML
    public void handleAddRandProduct(ActionEvent event) {
        if(isOpened){
            ConnectDB connectDB = new ConnectDB();
            ArrayList <String> barcodes = connectDB.getBarcodes();
            Random random = new Random();
            if (barcodes.size() == 0) {
                Dialogs.create()
                        .owner(this)
                        .title("")
                        .masthead("Оповестите кладовщика")
                        .message("Товар на складе закончился.")
                        .showWarning();



            } else {
                int numberProduct=random.nextInt(10000)%barcodes.size();

                Product product = connectDB.findProductBarcode(barcodes.get(numberProduct));
                connectDB.setCountProduct(barcodes.get(numberProduct), product.getCount() - 1);
                product.setCount(1);
                boolean firstProduct=true;
                for (int i=0;i<check.size();i++){
                    if (product.getBarcode().equals(check.get(i).getBarcode())){
                        check.get(i).setCount(check.get(i).getCount()+1);
                        firstProduct=false;
                        break;
                    }
                }
                if (firstProduct) check.add(product);
                tableCheck.setItems(check);
                updateResult();
            }
        }
        else {
            Dialogs.create()
                    .owner(this)
                    .title("Ошибка")
                    .masthead("Товар не может быть добавлен")
                    .message("Сначала откройте чек!")
                    .showWarning();
        }


    }

    @FXML
    public void handleEnterCount(ActionEvent event) {
        int number = -1;
        number = tableCheck.getSelectionModel().getSelectedIndex();
        System.out.println(""+number);
        if (number > -1) {
            int thisCount=check.get(number).getCount();
            Optional<String> response = Dialogs.create()
                    .owner(this)
                    .title("Ручное добавление товара")
                    .masthead("Введите новое количество товара")
                    .message("Введите количество:")
                    .showTextInput(String.valueOf(thisCount));
            if (response.isPresent()){
                try{

                    int newCount=Integer.parseUnsignedInt(response.get());
                    if (newCount<0) return;

                    if (newCount==0){
                        check.remove(number);
                        tableCheck.setItems(check);
                        updateResult();
                        return;
                    }
                    if (newCount<thisCount){
                        check.get(number).setCount(newCount);
                        updateResult();
                        return;
                    }
                    if (newCount>thisCount){
                        ConnectDB connectDB=new ConnectDB();
                        Product thisProduct=connectDB.findProductBarcode(check.get(number).getBarcode());
                        if (thisProduct.getCount()<newCount){
                            Action resp = Dialogs.create()
                                    .owner(this)
                                    .title("")
                                    .masthead("На складе осталось "+thisProduct.getCount()+" единиц товара :")
                                    .message("Забрать оставшиеся?")
                                    .actions(Dialog.ACTION_OK, Dialog.ACTION_CANCEL)
                                    .showConfirm();

                            if (resp == Dialog.ACTION_OK) {
                                check.get(number).setCount(check.get(number).getCount()+thisProduct.getCount());
                                connectDB.setCountProduct(thisProduct.getBarcode(), 0);
                                updateResult();

                            } else {
                                return;
                            }
                        } else {
                            connectDB.setCountProduct(thisProduct.getBarcode(), thisProduct.getCount() - (newCount - check.get(number).getCount()));
                            check.get(number).setCount(newCount);
                            updateResult();
                            return;
                        }
                    }
                } catch (NumberFormatException e){
                    Action resp2 = Dialogs.create()
                            .owner(this)
                            .title("")
                            .masthead("Некорректный ввод")
                            .message("Введите верное значение количества.")
                            .actions(Dialog.ACTION_OK, Dialog.ACTION_CANCEL)
                            .showConfirm();
                }
            }
        }
    }

    @FXML
    public void handleDeleteProduct(ActionEvent event) {
        if(isOpened) {
            int number = -1;
            number = tableCheck.getSelectionModel().getSelectedIndex();
            check.remove(number);
            tableCheck.setItems(check);
            updateResult();
        }
        else {
            Dialogs.create()
                    .owner(this)
                    .title("Ошибка")
                    .masthead("Чек не открыт")
                    .message("Сначала откройте чек!")
                    .showWarning();
        }
    }

    @FXML
    public void handleEnterBarcode(ActionEvent event) {

        if (isOpened) {
            Optional<String> response = Dialogs.create()
                    .owner(this)
                    .title("Ручное добавление товара")
                    .masthead("Введите штрих-код вручную")
                    .message("Введите штрих-код:")
                    .showTextInput("");
            if (response.isPresent()) {

                    System.out.println(response.get());
                    ConnectDB connectDB = new ConnectDB();
                    Product newProduct = connectDB.findProductBarcode(response.get());
                    if (newProduct != null) {
                        if (newProduct.getCount() == 0) {
                            Dialogs.create()
                                    .owner(this)
                                    .title("Предупреждение")
                                    .masthead("")
                                    .message("Товар не обнаружен на складе свяжитесь с кладовщиком")
                                    .showWarning();
                            connectDB.setCountProduct(newProduct.getBarcode(), newProduct.getCount() - 1);
                            newProduct.setCount(1);
                            boolean firstProduct = true;
                            for (int i = 0; i < check.size(); i++) {
                                if (newProduct.getBarcode().equals(check.get(i).getBarcode())) {
                                    check.get(i).setCount(check.get(i).getCount() + 1);
                                    firstProduct = false;
                                    break;
                                }
                            }
                            if (firstProduct) check.add(newProduct);
                            tableCheck.setItems(check);
                            updateResult();
                        } else {
                            connectDB.setCountProduct(newProduct.getBarcode(), newProduct.getCount() - 1);
                            newProduct.setCount(1);
                            boolean firstProduct = true;
                            for (int i = 0; i < check.size(); i++) {
                                if (newProduct.getBarcode().equals(check.get(i).getBarcode())) {
                                    check.get(i).setCount(check.get(i).getCount() + 1);
                                    firstProduct = false;
                                    break;
                                }
                            }
                            if (firstProduct) check.add(newProduct);
                            tableCheck.setItems(check);
                            updateResult();
                        }
                    }
                else{
                        Dialogs.create()
                                .owner(this)
                                .title("Ошибка")
                                .masthead("Товар не найден")
                                .message("Попробуйте ввести другой штрих-код")
                                .showWarning();
                }
            }

        }else {
            Dialogs.create()
                    .owner(this)
                    .title("")
                    .masthead("Операция невозможна")
                    .message("Чек не открыт!")
                    .showWarning();
        }


    }

    private double calculate() {
        double end = 0;
        if (isOpened) {
            for (int i = 0; i < check.size(); i++) {
                end += check.get(i).getEndSum();
            }
        }
        return end;
    }

    @FXML
    public void handleOpenCheck(ActionEvent event) {
        isOpened = true;
        labelCassEndMessage.setText("Итого");
        labelCassEndValue.setText("0.00 р.");
    }

    @FXML
    public void handleDiscountCard(ActionEvent event) {
        if(isOpened) {
            double discount;
            Random random = new Random();
            discount = random.nextDouble() * 100;
            if (maxDiscount<discount) maxDiscount=discount;
            DecimalFormat df = new DecimalFormat("##");
            df.setRoundingMode(RoundingMode.DOWN);
            labelCassEndMessage.setText( "Скидка по текущей карте :"+df.format(discount)+" %"+"  Итого со скидкой " + df.format(maxDiscount) + " % :");

            df = new DecimalFormat("##.##");
            df.setRoundingMode(RoundingMode.DOWN);
            labelCassEndValue.setText(df.format(calculate() * (1 - maxDiscount / 100)) + " р.");
        }
        else{
            Dialogs.create()
                    .owner(this)
                    .title("")
                    .masthead("Операция невозможна")
                    .message("Чек не открыт!")
                    .showWarning();
        }
    }

    @FXML
    public void handleCloseCheck(ActionEvent event) {
        isOpened = false;
        check.remove(0, check.size());
        tableCheck.setItems(check);
        labelCassEndMessage.setText("Откройте чек");
        labelCassEndValue.setText("");
        maxDiscount=0;
    }

    public void updateResult() {
        labelCassEndMessage.setText("Итого :");
        DecimalFormat df = new DecimalFormat("##.##");
        df.setRoundingMode(RoundingMode.DOWN);
        labelCassEndValue.setText(df.format(calculate()) + " р.");
    }


}
