package entity;

import javafx.beans.property.*;

/**
 * Created by Julia on 25.03.2015.
 */
public class Product {
    SimpleStringProperty barcode;
    SimpleStringProperty name;
    SimpleDoubleProperty cost;
    SimpleIntegerProperty count;
    SimpleDoubleProperty endSum;

    public Product(String barcode,String name, double cost,int count) {
        this.barcode=new SimpleStringProperty(barcode);
        this.name = new SimpleStringProperty(name);
        this.cost = new SimpleDoubleProperty(cost);
        this.count=new SimpleIntegerProperty(count);
        this.endSum=new SimpleDoubleProperty(Math.floor(this.cost.get() * this.count.get() * 100) / 100);
    }

    public double getEndSum() {
        return endSum.get();
    }

    public String getBarcode() {
        return barcode.get();
    }

    public void setBarcode(String barcode) {
        this.barcode.set(barcode);
    }

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public double getCost() {
        return cost.get();
    }

    public void setCost(double cost) {
        this.cost.set(cost);
        this.endSum.set(Math.floor(this.cost.get()*this.count.get()* 100) / 100);
    }

    public int getCount() {
        return count.get();
    }

    public void setCount(int count) {
        this.count.set(count);
        this.endSum.set(Math.floor(this.cost.get()*this.count.get()* 100) / 100);
    }

    public StringProperty barcodeProperty(){return barcode;}
    public StringProperty nameProperty(){return name;}
    public DoubleProperty costProperty(){return cost;}
    public IntegerProperty countProperty(){return count;}
    public DoubleProperty endSumProperty(){return endSum;}
}
