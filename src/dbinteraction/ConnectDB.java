package dbinteraction;

import entity.*;

import javax.swing.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by Xenomorf on 05.04.2015.
 */
public class ConnectDB {
    Connection conn;
    public ConnectDB() {
        String url = "jdbc:mysql://172.18.7.15:3306/";
        String dbName = "shop";
        String driver = "com.mysql.jdbc.Driver";
        String userName = "root";
        String password = "98897777";
        try {
            Class.forName(driver).newInstance();
            conn = DriverManager.getConnection(url + dbName, userName, password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }


    //Методы авторизации
    public boolean checkName(String name){
        String query="SELECT name FROM employees WHERE name=\'"+name+"\'";
        boolean hasName=false;
        try {
            Statement st=conn.createStatement();
            ResultSet resultSet=st.executeQuery(query);
            int rowCount=0;
            while (resultSet.next()){
                rowCount++;
            }
            if (rowCount==0) hasName=false;
            else hasName=true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            return hasName;
        }
    }
    public boolean checkPass(String name,String pass){
        String query="SELECT name FROM employees WHERE name = \""+name+"\" AND password = \""+pass+"\"";
        boolean passTrue=false;
        try {

            Statement st=conn.createStatement();
            ResultSet resultSet=st.executeQuery(query);
            int rowCount=0;
            while (resultSet.next()){
                rowCount++;
            }
            if (rowCount==0) passTrue=false;
            else passTrue=true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            return passTrue;
        }
    }
    public String getAccess(String name,String pass){
        String access="---";
        String query="SELECT access FROM employees WHERE name = \""+name+"\" AND password = \""+pass+"\"";
        try {
            Statement st=conn.createStatement();
            ResultSet resultSet=st.executeQuery(query);
            int rowCount=0;
            while (resultSet.next()){
                rowCount++;
                access=resultSet.getString("access");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            return access;
        }
    }
    //Методы для кассового аппарата
    public JTable getProductsTable(){
        String query="SELECT * FROM assortment";
        JTable jTable=new JTable();

        //Начало спионеренного
        ArrayList columnNames = new ArrayList();
        ArrayList data = new ArrayList();

        Statement stmt = null;
        try {
            stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery( query );
            ResultSetMetaData md = rs.getMetaData();
            int columns = md.getColumnCount();

            //  Get column names
            for (int i = 1; i <= columns; i++)
            {
                columnNames.add( md.getColumnName(i) );
            }

            //  Get row data
            while (rs.next())
            {
                ArrayList row = new ArrayList(columns);

                for (int i = 1; i <= columns; i++)
                {
                    row.add( rs.getObject(i) );
                }

                data.add( row );
            }

        Vector columnNamesVector = new Vector();
        Vector dataVector = new Vector();

        for (int i = 0; i < data.size(); i++)
        {
            ArrayList subArray = (ArrayList)data.get(i);
            Vector subVector = new Vector();
            for (int j = 0; j < subArray.size(); j++)
            {
                subVector.add(subArray.get(j));
            }
            dataVector.add(subVector);
        }

        for (int i = 0; i < columnNames.size(); i++ )
            columnNamesVector.add(columnNames.get(i));

        //  Create table with database data

        jTable = new JTable(dataVector, columnNamesVector)
        {
            public Class getColumnClass(int column)
            {
                for (int row = 0; row < getRowCount(); row++)
                {
                    Object o = getValueAt(row, column);

                    if (o != null)
                    {
                        return o.getClass();
                    }
                }

                return Object.class;
            }
        };

        } catch (SQLException e) {
            e.printStackTrace();
        }
        //Закат коммунизма





        return jTable;
    }
    public  Product  findProductBarcode(String barcode){
        Product product=null;
        String query="Select barcode,name,count,cost FROM assortment WHERE barcode =\""+barcode+"\"";
        Statement st= null;
        try {
            st = conn.createStatement();
            ResultSet resultSet=st.executeQuery(query);
            if (resultSet.next()){
                product=new Product(resultSet.getString("barcode"),resultSet.getString("name"),resultSet.getDouble("cost"),resultSet.getInt("count"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return product;
    }

    public ArrayList<String> getBarcodes(){
        ArrayList<String> barcodes=new ArrayList<String>();
        String query="SELECT barcode FROM assortment WHERE count>0";
        Statement st= null;
        try {
            st = conn.createStatement();
            ResultSet resultSet=st.executeQuery(query);
            while (resultSet.next()){
                barcodes.add(resultSet.getString("barcode"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return barcodes;
    }

    public void setCountProduct(String barcode, int count){
        String query="UPDATE assortment SET count=? WHERE barcode=?";
        try {
            Statement st=conn.createStatement();
            PreparedStatement preparedStmt = conn.prepareStatement(query);
            preparedStmt.setInt   (1, count);
            preparedStmt.setString(2, barcode);
            preparedStmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Product> findProductsByName(String name){
        ArrayList<Product> result=new ArrayList<Product>();
        String query="SELECT barcode,name,count,cost FROM assortment WHERE name =\""+name+"\"";
        Statement st= null;
        try {
            st = conn.createStatement();
            ResultSet resultSet=st.executeQuery(query);
            while (resultSet.next()){
                result.add(new Product(resultSet.getString("barcode"),resultSet.getString("name"),resultSet.getDouble("cost"),resultSet.getInt("count")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }

    public void deleteProduct(String barcode){
        String query="DELETE FROM assortment WHERE   barcode = ?";
        PreparedStatement preparedStmt = null;
        try {
            preparedStmt = conn.prepareStatement(query);
            preparedStmt.setString(1, barcode);
            preparedStmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        // execute the preparedstatement

    }

    public boolean insertProduct(Product product){
        String query = " insert into assortment (barcode, name, count, cost)"
                + " values (?, ?, ?, ?)";

        // create the mysql insert preparedstatement
        PreparedStatement preparedStmt = null;
        if(product.getBarcode().length()!=13) return false;
        try {
            preparedStmt = conn.prepareStatement(query);

        preparedStmt.setString(1, product.getBarcode());
        preparedStmt.setString(2, product.getName());
        preparedStmt.setInt(3, product.getCount());
        preparedStmt.setDouble(4, product.getCost());

        // execute the preparedstatement

            return preparedStmt.execute();
        } catch (SQLException e) {
        e.printStackTrace();
            return false;
    }
    }

    public ArrayList<Product> selectAll(){
        ArrayList<Product> result=new ArrayList<Product>();
        String query="SELECT barcode,name,count,cost FROM assortment";
        Statement st= null;
        try {
            st = conn.createStatement();
            ResultSet resultSet=st.executeQuery(query);
            while (resultSet.next()){
                result.add(new Product(resultSet.getString("barcode"),resultSet.getString("name"),resultSet.getDouble("cost"),resultSet.getInt("count")));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return result;
    }




    public static  void main(String[] args){
        ConnectDB connectDB=new ConnectDB();
        if (connectDB.checkName("cashier")) System.out.println("Есть кассир");
        if (connectDB.checkPass("cashier", "9889") ) System.out.println("Кассир зашёл с правильным паролем и имеет права доступа : "+connectDB.getAccess("cashier","9889"));
    }
}
