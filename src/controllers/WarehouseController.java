package controllers;

import dbinteraction.ConnectDB;
import entity.Product;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import org.controlsfx.control.*;
import org.controlsfx.control.ButtonBar;
import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialogs;
import org.controlsfx.dialog.Dialog;

import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.function.Consumer;

/**
 * Created by Xenomorf on 08.04.2015.
 */
public class WarehouseController extends VBox{
    @FXML
    Button buttonFind;
    @FXML
    Button buttonReset;
    @FXML
    Button buttonAddProduct;
    @FXML
    Button buttonDeleteProduct;
    @FXML
    TextField textFindProduct;
    @FXML
    TableView<Product> tableProducts;
    @FXML
    RadioButton rbBarcode;
    @FXML
    RadioButton rbName;
    @FXML
    ToggleGroup warehouseTypeFind;
    @FXML
    Label labelNameWarehouser;

    private ObservableList<Product> products = FXCollections.observableArrayList();

    public WarehouseController(){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/views/WarehouseGUI.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
            labelNameWarehouser.setText(LoginController.USER_NAME);
            tableProducts.setEditable(false);
            tableProducts.getColumns().remove(0, 2);
            TableColumn barcodeColumn = new TableColumn("Штрих-код");
            barcodeColumn.setPrefWidth(100);
            barcodeColumn.setResizable(false);
            barcodeColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("barcode"));
            TableColumn nameColumn = new TableColumn("Название");
            nameColumn.setPrefWidth(150);
            nameColumn.setResizable(false);
            nameColumn.setCellValueFactory(new PropertyValueFactory<Product, String>("name"));
            TableColumn priceColumn = new TableColumn("Цена");
            priceColumn.setPrefWidth(60);
            priceColumn.setResizable(false);
            priceColumn.setCellValueFactory(new PropertyValueFactory<Product, Double>("cost"));
            TableColumn countColumn = new TableColumn("Количество");
            countColumn.setPrefWidth(100);
            countColumn.setResizable(false);
            countColumn.setCellValueFactory(new PropertyValueFactory<Product, Integer>("count"));

            tableProducts.getColumns().add(barcodeColumn);
            tableProducts.getColumns().add(nameColumn);
            tableProducts.getColumns().add(priceColumn);
            tableProducts.getColumns().add(countColumn);

            ConnectDB connectDB=new ConnectDB();

            products.remove(0,products.size());
            ArrayList<Product> allProducts=connectDB.selectAll();
            for (Product x : allProducts) {
                products.add(x);
            }
            tableProducts.setItems(products);
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }

    @FXML
    public void handleFindProduct(){
        if (rbBarcode.isSelected()){
            products.remove(0,products.size());
            String barcode=textFindProduct.getText();

            ConnectDB connectDB=new ConnectDB();
            Product findedProduct=connectDB.findProductBarcode(barcode);
            System.out.println(barcode);
            if(findedProduct!=null){
                products.add(findedProduct);
                tableProducts.setItems(products);
            } else {
                Dialogs.create()
                        .owner(this)
                        .title("Информация")
                        .masthead("Товар не найден")
                        .message("Введите другой штрих-код.")
                        .showInformation();
            }
        }else {

            products.remove(0,products.size());
            String barname=textFindProduct.getText();
            ConnectDB connectDB=new ConnectDB();
            ArrayList<Product> findedProduct=connectDB.findProductsByName(barname);
            if(findedProduct.size()!= 0) {
                for (Product x : findedProduct) {
                    products.add(x);
                }
                tableProducts.setItems(products);
            }
            else {
                Dialogs.create()
                        .owner(this)
                        .title("Информация")
                        .masthead("Товар не найден")
                        .message("Введите другое имя.")
                        .showInformation();
            }
        }
    }
    @FXML
    public  void  handleDeleteProduct(){

        int number = -1;
        number = tableProducts.getSelectionModel().getSelectedIndex();

        if (number>-1){
            ConnectDB connectDB=new ConnectDB();
            connectDB.deleteProduct(tableProducts.getItems().get(number).getBarcode());
            products.remove(0,products.size());

            ArrayList<Product> allProducts=connectDB.selectAll();
            for (Product x : allProducts) {
                products.add(x);
            }
            tableProducts.setItems(products);


        }

    }
    @FXML
    public void handleResetTable() {
        ConnectDB connectDB=new ConnectDB();
        products.remove(0,products.size());
        ArrayList<Product> allProducts=connectDB.selectAll();
        for (Product x : allProducts) {
            products.add(x);
        }
        tableProducts.setItems(products);
    }

    final TextField barcodeField = new TextField();
    final TextField nameField = new TextField();
    final TextField countField = new TextField();
    final TextField costField = new TextField();
    final Action actionLogin = new Action("Добавить", new Consumer<ActionEvent>() {
        @Override
        public void accept(ActionEvent actionEvent) {

            String barcode=barcodeField.getText();
            String name=nameField.getText();
            String count=countField.getText();
            String cost=costField.getText();
            System.out.println(barcodeField.getText());
            Product newProduct=new Product(barcode,name,Double.parseDouble(cost),Integer.parseInt(count));
            ConnectDB addcdb=new ConnectDB();
            addcdb.insertProduct(newProduct);
            Dialog dlg = (Dialog) actionEvent.getSource();
            // real login code here
            dlg.hide();
        }
    });


    public void handleAddToTable() {
        ConnectDB connectDB=new ConnectDB();
        Dialog dlg = new Dialog(this, "Добавление товара");



        GridPane grid = new GridPane();
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(0, 10, 0, 10));

        barcodeField.setPromptText("Штрих-код");
        nameField.setPromptText("Имя");
        countField.setPromptText("Количество");
        costField.setPromptText("Стоимость");

        grid.add(new Label("Штрих-код:"), 0, 0);
        grid.add(barcodeField, 1, 0);
        grid.add(new Label("Имя:"), 0, 1);
        grid.add(nameField, 1, 1);

        grid.add(new Label("Количество:"), 0, 2);
        grid.add(countField, 1, 2);
        grid.add(new Label("Стоимость:"), 0, 3);
        grid.add(costField, 1, 3);



        //ButtonGroup(actionLogin, ButtonType.OK);
        actionLogin.disabledProperty().set(true);

        // Do some validation (using the Java 8 lambda syntax).
        barcodeField.textProperty().addListener((observable, oldValue, newValue) -> {
            actionLogin.disabledProperty().set(newValue.trim().isEmpty());
        });

        dlg.setMasthead("Введите параметры товара");
        dlg.setContent(grid);
        dlg.getActions().addAll(actionLogin, Dialog.ACTION_CANCEL);

        // Request focus on the username field by default.
        Platform.runLater(() -> barcodeField.requestFocus());

        dlg.show();
    }




}
