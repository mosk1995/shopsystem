package controllers;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Xenomorf on 20.05.2015.
 */
public class HelpController extends SplitPane{
    @FXML
    WebView webView;
    @FXML
    TreeView<String> treeView;

    private String[][] names=new String[4][];

    {
        names[0]=new String[3];
        names[0][0]=new String("Работа с авторизацией");
        names[0][1]=new String("Описание окна авторизации");
        names[0][2]=new String("Осуществление авторизации");
        names[1]=new String[7];
        names[1][0]=new String("Руководство по использованию кассового аппарата");
        names[1][1]=new String("Описание окна кассира");
        names[1][2]=new String("Осуществление работы с кассовым аппаратом");
        names[1][3]=new String("Работа с чеком");
        names[1][4]=new String("Работа с товарами");
        names[1][5]=new String("Ручной ввод количества/штрих-кода");
        names[1][6]=new String("Работа со скидочными картами");
        names[2]=new String[5];
        names[2][0]=new String("Руководство по использованию модуля склада");
        names[2][1]=new String("Описание окна кладовщика");
        names[2][2]=new String("Поиск товара в базе данных");
        names[2][3]=new String("Добавление товара в базу данных");
        names[2][4]=new String("Удаление товара из базы данных");
        names[3]=new String[3];
        names[3][0]=new String("Руководство по использованию модуля мерчендайзера");
        names[3][1]=new String("Описание окна мерчендайзера");
        names[3][2]=new String("Списание товара");


    }

    public HelpController(){
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/views/HelpGUI.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
            WebEngine webEngine=webView.getEngine();

            TreeItem<String> rootItem = new TreeItem<>("Руководство пользователя");
            rootItem.setExpanded(true);

            //Сектор страниц с авторизацией
            TreeItem<String> itemAuth = new TreeItem<>(names[0][0]);
            itemAuth.setExpanded(true);
            rootItem.getChildren().add(itemAuth);

            TreeItem<String> authPage = new TreeItem<>(names[0][1]);
            itemAuth.getChildren().add(authPage);
            authPage = new TreeItem<>(names[0][2]);
            itemAuth.getChildren().add(authPage);

            //Сектор страниц с кассовым аппаратом
            TreeItem<String> itemCass = new TreeItem<>(names[1][0]);
            itemCass.setExpanded(true);
            rootItem.getChildren().add(itemCass);

            TreeItem<String> cassPage = new TreeItem<>(names[1][1]);
            itemCass.getChildren().add(cassPage);
            cassPage = new TreeItem<>(names[1][2]);
            itemCass.getChildren().add(cassPage);
            cassPage = new TreeItem<>(names[1][3]);
            itemCass.getChildren().add(cassPage);
            cassPage = new TreeItem<>(names[1][4]);
            itemCass.getChildren().add(cassPage);
            cassPage = new TreeItem<>(names[1][5]);
            itemCass.getChildren().add(cassPage);
            cassPage = new TreeItem<>(names[1][6]);
            itemCass.getChildren().add(cassPage);

            //Сектор страниц со складом
            TreeItem<String> itemWarehouse = new TreeItem<>(names[2][0]);
            itemWarehouse.setExpanded(true);
            rootItem.getChildren().add(itemWarehouse);

            TreeItem<String> warehousePage = new TreeItem<>(names[2][1]);
            itemWarehouse.getChildren().add(warehousePage);
            warehousePage = new TreeItem<>(names[2][2]);
            itemWarehouse.getChildren().add(warehousePage);
            warehousePage = new TreeItem<>(names[2][3]);
            itemWarehouse.getChildren().add(warehousePage);
            warehousePage = new TreeItem<>(names[2][4]);
            itemWarehouse.getChildren().add(warehousePage);

            //Сектор страниц с мерчендайзером
            TreeItem<String> itemMerch = new TreeItem<>(names[3][0]);
            itemMerch.setExpanded(true);
            rootItem.getChildren().add(itemMerch);

            TreeItem<String> merchPage = new TreeItem<>(names[3][1]);
            itemMerch.getChildren().add(merchPage);
            merchPage = new TreeItem<>(names[3][2]);
            itemMerch.getChildren().add(merchPage);


            treeView.setRoot(rootItem);


            treeView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<TreeItem<String>>() {
                @Override
                public void changed(ObservableValue<? extends TreeItem<String>> observable, TreeItem<String> oldValue, TreeItem<String> newValue) {

                    TreeItem<String> selectedItem = (TreeItem<String>) newValue;

                    String path = System.getProperty("user.dir");
                    path.replace("\\\\", "/");
                    if (selectedItem.getValue().equals("Руководство пользователя"))   path +=  "\\src\\help\\main.html";

                    if (selectedItem.getValue().equals(names[0][0]))   path +=  "\\src\\help\\auth\\main.html";
                    if (selectedItem.getValue().equals(names[0][1]))   path +=  "\\src\\help\\auth\\a1.html";
                    if (selectedItem.getValue().equals(names[0][2]))   path +=  "\\src\\help\\auth\\a2.html";

                    if (selectedItem.getValue().equals(names[1][0]))   path +=  "\\src\\help\\cass\\main.html";
                    if (selectedItem.getValue().equals(names[1][1]))   path +=  "\\src\\help\\cass\\c1.html";
                    if (selectedItem.getValue().equals(names[1][2]))   path +=  "\\src\\help\\cass\\c2.html";
                    if (selectedItem.getValue().equals(names[1][3]))   path +=  "\\src\\help\\cass\\c3.html";
                    if (selectedItem.getValue().equals(names[1][4]))   path +=  "\\src\\help\\cass\\c4.html";
                    if (selectedItem.getValue().equals(names[1][5]))   path +=  "\\src\\help\\cass\\c5.html";
                    if (selectedItem.getValue().equals(names[1][6]))   path +=  "\\src\\help\\cass\\c6.html";


                    if (selectedItem.getValue().equals(names[3][0]))   path +=  "\\src\\help\\merch\\main.html";
                    if (selectedItem.getValue().equals(names[3][1]))   path +=  "\\src\\help\\merch\\m1.html";
                    if (selectedItem.getValue().equals(names[3][2]))   path +=  "\\src\\help\\merch\\m2.html";

                    if (selectedItem.getValue().equals(names[2][0]))   path +=  "\\src\\help\\wh\\main.html";
                    if (selectedItem.getValue().equals(names[2][1]))   path +=  "\\src\\help\\wh\\w1.html";
                    if (selectedItem.getValue().equals(names[2][2]))   path +=  "\\src\\help\\wh\\w2.html";
                    if (selectedItem.getValue().equals(names[2][3]))   path +=  "\\src\\help\\wh\\w3.html";
                    if (selectedItem.getValue().equals(names[2][4]))   path +=  "\\src\\help\\wh\\w4.html";



                    System.out.println(path);
                    webEngine.load("file:///" + path);

                }
            });

        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
    }


}
