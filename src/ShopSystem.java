import controllers.LoginController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class ShopSystem extends Application {

    public static String USER_NAME="ANANAS";

    @Override
    public void start(Stage stage) throws Exception{
        LoginController loginController=new LoginController();
        stage.setScene(new Scene(loginController));
        stage.setTitle("Авторизация");
        stage.setWidth(300);
        stage.setHeight(350);
        stage.setResizable(false);
        stage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
